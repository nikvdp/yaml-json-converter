Simple CLI utilities to convert between yaml and json.

# Installation

### Prereqs
Have Go installed. If you don't, easeist way is

``` bash
brew install go
```


### Clone source
``` bash
mkdir -p ~/go/src/yaml-json-converter
git clone git@bitbucket.org:nikvdp/yaml-json-converter.git ~/go/src/yaml-json-converter
```

### Build executable for y2j util (yaml2json)
``` bash
cd ~/go/src/yaml-json-converter/y2j
go get -d
go build
```

### (optional) add it to path for easy access
Add to path
``` bash
sudo ln -s ~/go/src/yaml-json-converter/y2j/y2j /usr/local/bin/y2j  
```

### Build executable for j2y util (json2yaml)
cd ~/go/src/yaml-json-converter/j2y
go get -d
go build

### (optional) add it to path for easy access
sudo ln -s ~/go/src/yaml-json-converter/j2y/j2y /usr/local/bin/j2y  

# Usage 

`y2j` and `j2y` both take input on standard in, and output to stdout. 

To convert/validate a yaml file to json, do:

``` bash
cat <yaml-file> | y2j
```

And to convert json to yaml:

``` bash
cat <json-file> | j2y
```

It's often convenient to use with yaml/json that is on the clipboard as well:

``` sh
# with yaml on the clipboard on OS X
pbpaste | y2j
```
