module bitbucket.org/nikvdp/yaml-json-converter

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/ghodss/yaml v1.0.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
