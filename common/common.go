package common

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/ghodss/yaml"
	"log"
	"os"
)

func readStdin() string {
	scanner := bufio.NewScanner(os.Stdin)
	stdIn := ""
	for scanner.Scan() {
		stdIn += scanner.Text() + "\n"
		//fmt.Println(scanner.Text())
	}
	if scanner.Err() != nil {
		// handle error.
	}

	return stdIn
}

func Y2j() {
	inpString := readStdin()
	var unmarshalled interface{}
	err := yaml.Unmarshal([]byte(inpString), &unmarshalled)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	jsoned, err := json.MarshalIndent(unmarshalled.(map[string]interface{}), "",
		"  ")
	fmt.Printf(string(jsoned))
}

func J2y() {
	inpString := readStdin()
	var unmarshalled interface{}
	err := json.Unmarshal([]byte(inpString), &unmarshalled)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	yamled, err := yaml.Marshal(unmarshalled.(map[string]interface{}))
	fmt.Printf(string(yamled))

}

func nothing() {
	inpString := []byte(`{"Name":"Wednesday","Age":6,"Parents":["Gomez","Morticia"]}`)
	fmt.Printf("b (type): %T\n", inpString)

	var unmarshalled interface{}
	err := yaml.Unmarshal(inpString, &unmarshalled)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	//mapped := f.(map[interface {}]interface{})
	fmt.Printf("f (type): %T\n", unmarshalled)

	jsoned, err := json.Marshal(unmarshalled.(map[string]interface{}))
	fmt.Printf(">>> Parsed JSON: '%s'\n", jsoned)

	fmt.Printf("jsoned: (type): %T\n", jsoned)

	spew.Dump(unmarshalled)
	//printMap(mapped)

	//fmt.Printf("age val: %s\n", f.(map[string]interface{}))
}

